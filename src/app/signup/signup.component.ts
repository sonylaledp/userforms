import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  user: any;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"; 

  constructor(private route: Router) { }

  ngOnInit(): void {
    this.user = {
      userName: '',
      officialEmail: '',
      password: '',
      confirmPassword: ''
    }
  }
  onSubmitClick()
  {
    
    (this.user as any)["submitted"] = true;
    console.log(this.user);
  }
  saveData(){
    console.log(this.user)
    sessionStorage.setItem('userData',JSON.stringify(this.user));
    this.route.navigate(['/']);
  }

}
