import { FormtestComponent } from './formtest/formtest.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path:"",component:HomeComponent}, 
  { path:"login",component:LoginComponent},
  { path:"signup",component:SignupComponent},
  { path:"formtest",component:FormtestComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
